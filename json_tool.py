#!/usr/bin/env python3
"""Dump or expand json"""
import sys
import fileinput
import json

def read_input(source=None):
    """Read input data (from file or from stdin)"""
    text = ''
    try:
        with fileinput.input(files=source) as src:
            for line in src:
                text = ''.join([text, line])
    except Exception as ex:
        print(f"Error to read the source: {ex}", file=sys.stderr)
        sys.exit(1)
    try:
        _json = json.loads(text)
    except Exception as ex:
        print(f"The text is not a valid json: {ex}", file=sys.stderr)
        sys.exit(1)
    return _json

def compress():
    _json = read_input(source=sys.argv[1:] if sys.argv[1:] else None)
    print(json.dumps(_json, indent=None, separators=(',', ':')))

def extend():
    _json = read_input(source=sys.argv[1:] if sys.argv[1:] else None)
    print(json.dumps(_json, indent=4, sort_keys=True))
